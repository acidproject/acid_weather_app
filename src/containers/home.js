import { connect } from 'react-redux'
import HomeScreen from '../screens/home';
import { getWeatherInformation } from '../actions/weatherActions';

const mapStateToProps = ({weather}) => {
    return {
        ...weather
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        getWeatherInformation: (lat, lng, countryName) => {
            dispatch(getWeatherInformation(lat, lng, countryName))
        },
    }
  }
  
const Home = connect(mapStateToProps,mapDispatchToProps)(HomeScreen)

export default Home