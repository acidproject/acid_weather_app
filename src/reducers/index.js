import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import weather from './weatherReducer';

//Combine reducers for be used in mapStateToProps
const reducers = combineReducers({
  weather,
  routing: routerReducer
})

export default reducers