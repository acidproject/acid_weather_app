import constants from '../constants'

//Define a default state
const defaultState = {
    isFetching: false,
    weather: {}
}

//Handling states
const weather = (state = defaultState, action) => {
  switch (action.type) {
    case constants.GET_WEATHER_INFORMATION:
      return { ...state, isFetching: true }
    case constants.GET_WEATHER_INFORMATION_FETCHED:
      return { ...state, isFetching: false, weather: action.payload.weather}
    default:
      return state
  }
}

export default weather