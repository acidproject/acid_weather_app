import Axios from 'axios';

function getDefaultHeaders(contentType, accept) {
  return {
    'Content-Type': contentType || 'application/json',
    'Accept': accept || 'application/json'
  };
}

function getHeaders(contentType, accept) {
  return getDefaultHeaders(contentType, accept);
}

export const doRequest = (req) => {  
  req.headers = getHeaders();
  return new Promise((resolve, reject) => {
    Axios(req).then((response) => {
      resolve(response);
    }).catch((error) => {
      reject(error);
    });
  });
};

const getBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.onerror = (error) => {
      reject(error);
    };
  });
};

export const uploadFiles = (req, files) => {
  let error = null;
  return new Promise((resolve, reject) => {
    files.forEach((file, index) => {
      getBase64(file)
        .then((encodedData) => {
          const _req = Object.assign(req, {
            headers: getHeaders(),
            data: {
              lastModified: file.lastModified, // 1446695019000
              name: file.name, // "PRINT THIS - Your two-factor authentication codes.html"
              size: file.size, // 30702
              type: file.type, // "text/html"
              data: encodedData
            }
          });
          Axios(_req).then(() => {
            if (index === files.length - 1 && !error) {
              resolve(true);
            } else if (error) {
              reject(error);
            }
          }).catch((err) => {
            if (index === files.length - 1) {
              reject(err);
            } else {
              error = err;
            }
          });
        })
        .catch((err) => {
          if (index === files.length - 1) {
            reject(err);
          } else {
            error = err;
          }
        }); // prints the base64 string
    });
  });
};
