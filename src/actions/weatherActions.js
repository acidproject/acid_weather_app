import constants from '../constants'

//Action for get information in the api
export const getWeatherInformation = (lat, lng, countryName) => {
    return {
        type: constants.GET_WEATHER_INFORMATION,
        payload: {
            lat,
            lng,
            countryName
        }
    }
}

//action when the api returns its result
export const getWeatherInformationFetched = (weather) => {
    return {
        type: constants.GET_WEATHER_INFORMATION_FETCHED,
        payload: {
            weather
        }
    }
}