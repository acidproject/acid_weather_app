import React from "react"
import { compose, withProps } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps"
import mapStyles from "../../src/screens/home/mapStyles";

const mapOptions = {
    styles: mapStyles, 
    fullscreenControl: false, 
    disableDoubleClickZoom: true,  
    controlSize: true, 
    scrollwheel: false,
    gestureHandling: 'greedy'
}

//This component without state render the map in the application
const MyMapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyDf-yIqxErTkbWzKhLox7nAANnrfDIY190",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `100%` }} />,
        mapElement: <div style={{ height: `100%` }} />,
    }),
    withScriptjs,
    withGoogleMap
    )((props) => {
        return(
            <GoogleMap defaultOptions={mapOptions} defaultZoom={2} defaultCenter={{ lat: 41.015137, lng: 28.979530 }}>
                {props.polygons.map((polygon, i) => {
                    return(
                        <div key={i}>
                            {polygon}
                        </div>
                    )
                })}
            </GoogleMap>
        )
    }
    )

export default MyMapComponent;