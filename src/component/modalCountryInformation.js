import React from 'react'
import { Modal } from 'semantic-ui-react'

const ModalCountryInformation = (props) => {
    return(
        <Modal size='small' open={props.isOpen} onClose={() => props.onClose()} closeIcon>
            <Modal.Header>{props.countryName}</Modal.Header>
            <Modal.Content image>
            <Modal.Description>
                <div>
                    <p>Temperatura: {props.weather.temperature || 0}°</p>
                    <p>Resumen: {props.weather.summary}</p>
                    <p>Estación de año: {props.weather.currentSeasson}</p>
                </div>
            </Modal.Description>
            </Modal.Content>
        </Modal>
    )
}

export default ModalCountryInformation