import { fork, all } from 'redux-saga/effects'
import {getWeatherInformationSaga} from './weatherSagas';

//Define sagas forks
export default function * root() {
  yield all([
    fork(getWeatherInformationSaga)
  ])
}