import constants from '../constants'
import { put, takeEvery} from 'redux-saga/effects'
import * as apiCall from '../api/req';
import * as apiRoutes from '../constants/apiRoutes';
import { getWeatherInformationFetched } from '../actions/weatherActions';
 
//Function sagas to export
export function * getWeatherInformationSaga() {
    yield takeEvery(constants.GET_WEATHER_INFORMATION, getWeatherInformation)
}

//Intermediary officer who calls the api and gets its result 
function * getWeatherInformation(action) {
    let {lat, lng, countryName} = action.payload;

    try {
        const result = yield (apiCall.doRequest({
            method: 'GET',
            url: apiRoutes.WEATHER_URL.replace(apiRoutes.LAT, lat).replace(apiRoutes.LNG, lng).replace(apiRoutes.COUNTRY_NAME, countryName)
        }))
        yield put(getWeatherInformationFetched(result.data))
    } catch(e) {
        console.log(`error api ${e.statusCode}`)
        console.log(e)
    }
}