import React from 'react';
import ReactDOM from 'react-dom';
import createSagaMiddleware from 'redux-saga'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import { BrowserRouter } from 'react-router-dom'


import saga from './sagas'
import reducers from './reducers'


import { composeWithDevTools } from 'redux-devtools-extension';

import './index.css';
import App from './routes';
import * as serviceWorker from './serviceWorker';


const middlewares = [];

const sagaMiddleware = createSagaMiddleware();
middlewares.push(sagaMiddleware);

// if (process.env.NODE_ENV !== 'production') {
const loggerMiddleware = createLogger();
middlewares.push(loggerMiddleware)
// }

const composeEnhancers = composeWithDevTools({
  // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

const genericStoreEnhancer = applyMiddleware(...middlewares);
const store = createStore(reducers,  composeEnhancers(
  genericStoreEnhancer
  // other store enhancers if any
));
  
  

sagaMiddleware.run(saga);


ReactDOM.render(    
    <Provider store={store}>
        <BrowserRouter >          
          <App/>
        </BrowserRouter>    
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
