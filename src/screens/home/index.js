import React, { Component, useState } from 'react';
import { Polygon } from 'react-google-maps';
import MyMapComponent from '../../component/myMapComponent';
import ModalCountryInformation from '../../component/modalCountryInformation';


 class Home extends Component {
    
    constructor(props) {
        super(props)
        this.state = {
            polygons: [],
            isOpen: false,
            countryName: '',
            lat: '',
            lng: ''
        }
        window.initMap = this.initMap.bind(this);
    }
    

    componentDidMount () {
        const script = document.createElement('script');
        script.src = 'https://www.googleapis.com/fusiontables/v1/query?sql=SELECT%20name%2C%20kml_4326%20FROM%201foc3xO9DyfSIF6ofvN0kp2bxSfSeKog5FbdWdQ&callback=initMap&key=AIzaSyAm9yWCV7JPCTHCJut8whOjARd7pwROFDQ';
        var body = document.getElementsByTagName('body')[0];
        body.appendChild(script);
    }

    //triggers the call to the api to get the information
    handleClickOpenModal(lat, lng, countryName) {
        this.percentageChance()
        this.props.getWeatherInformation(lat,lng,countryName)
        this.setState({isOpen: true, countryName})
    }

    //Recursive method with 10 percent chance of failure
    percentageChance(){
        let number = Math.random() * 100;
        if(number < 10) {
            this.percentageChance()
        }
    }

    //Close modal
    onClose = () => {
        this.setState({isOpen: false})
    }

    //create map based on fusion tables kml
    initMap(data) {
        var colors = ['#FF0000', '#00FF00', '#0000FF', '#FFFF00'];
        var rows = data['rows'];
        let arr = []
        for (var i in rows) {
            if (rows[i][0] != 'Antarctica') {
                var newCoordinates = [];
                var geometries = rows[i][1]['geometries'];
                if (geometries) {
                    for (var j in geometries) {
                    newCoordinates.push(this.constructNewCoordinates(geometries[j]));
                    }
                } else {
                    newCoordinates = this.constructNewCoordinates(rows[i][1]['geometry']);
                }
                var randomnumber = Math.floor(Math.random() * 4);
                let countryName = rows[i][0]
                var country = <Polygon 
                                paths={newCoordinates} 
                                strokeColor={colors[randomnumber]} 
                                strokeOpacity={1}
                                strokeWeight={0.3}
                                fillColor={colors[randomnumber]}
                                fillOpacity={0}
                                name={rows[i][0]}
                                onClick={(e) => this.handleClickOpenModal(e.latLng.lat(), e.latLng.lng(), countryName)}
                                />

                arr.push(country)
            }
        }

        this.setState({polygons: this.state.polygons.concat(arr)})
    }

    //Asign coordinates to polygon
    constructNewCoordinates(polygon) {
        var newCoordinates = [];
        var coordinates = polygon['coordinates'][0];
        for (var i in coordinates) {
            newCoordinates.push({lat: coordinates[i][1], lng: coordinates[i][0]});
        }
        return newCoordinates;
      }
    
    
    //Render layout
    render() {
        return(
            <div style={{width: '100vw', height: '100vh'}}>
                <ModalCountryInformation isOpen={this.state.isOpen} onClose={this.onClose} countryName={this.state.countryName} weather={this.props.weather}/>
                <MyMapComponent isMarkerShown={true} polygons={this.state.polygons}/>
            </div>
        )
    }
}

export default Home;