let SERVER = 'https://agile-beyond-79575.herokuapp.com/api';

if (process.env.NODE_ENV === 'production') {
  SERVER = 'URL_PROUCTION.cl';
}

export const LAT = '/:lat';
export const LNG = '/:lng';
export const COUNTRY_NAME= '/:countryName'


//Constants for call server
export const WEATHER_URL = `${SERVER}/weather?lat=${LAT}&lng=${LNG}&countryName=${COUNTRY_NAME}`



