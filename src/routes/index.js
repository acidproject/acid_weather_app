import React from 'react';
import Home from '../containers/home';

import { BrowserRouter, Route } from 'react-router-dom';

//Return the principal router of the application
function App() {
  return (
    <div className="App" > 
        <BrowserRouter>
          <div>
            <Route exact path="/" component={Home}/>
          </div>
        </BrowserRouter>
    </div>
  );
}

export default App;
